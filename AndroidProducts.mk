
PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/poplar.mk

COMMON_LUNCH_CHOICES := \
    poplar-trunk_staging-eng \
    poplar-trunk_staging-user \
    poplar-trunk_staging-userdebug
